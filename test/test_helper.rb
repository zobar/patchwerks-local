require 'simplecov'

SimpleCov.start 'rails'

ENV['RAILS_ENV'] = 'test'
require File.expand_path('../dummy/config/environment', __FILE__)

require 'minitest/autorun'
require 'minitest/rails'

Excon.defaults[:mock] = true
Fog.mock!

class MiniTest::Rails::ActiveSupport::TestCase
  fixtures :all
end
