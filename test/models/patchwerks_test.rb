require 'test_helper'

describe Patchwerks do

  subject do
    Patchwerks
  end

  it "can retrieve the environment's config" do
    subject.config.must_be_kind_of ActiveSupport::HashWithIndifferentAccess
  end

  it 'can find the config directory' do
    subject.config_dir.must_equal File.join(Rails.root, 'config')
  end

  it 'can find a config file' do
    subject.config_file('test').must_equal File.join(Rails.root, 'config',
                                                     'test')
  end

end
