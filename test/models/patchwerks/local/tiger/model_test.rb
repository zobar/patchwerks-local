require 'test_helper'

describe Patchwerks::Local::Tiger::Model do

  describe 'class methods' do
    subject do
      Patchwerks::Local::Tiger::State
    end

    it 'responds to for_geoid' do
      state = subject.for_geoid('00').first_or_initialize
      state.geoid.must_equal '00'
    end

    describe 'for_record' do
      let :attributes do
        {
          'foo'      => 'Bar',
          'geoid'    => '00',
          'intptlat' => intpt.y,
          'intptlon' => intpt.x,
          'name'     => 'Baz',
          'region'   => ''}
      end

      let :factory do
        RGeo::Cartesian.preferred_factory srid: 4326
      end

      let :geometry do
        factory.multi_polygon([
          factory.polygon(
            factory.linear_ring([
              factory.point(-139.474624, 27.919623),
              factory.point(-140.474624, 27.919623),
              factory.point(-140.474624, 28.919623),
              factory.point(-139.474624, 28.919623)]))])
      end

      let :intpt do
        factory.point -140, 28
      end

      let :record do
        RGeo::Shapefile::Reader::Record.new 0, geometry, attributes
      end

      subject do
        Patchwerks::Local::Tiger::State.for_record record
      end

      it 'loads geoid' do
        subject.geoid.must_equal '00'
      end

      it 'loads general attributes' do
        subject.name.must_equal 'Baz'
      end

      it "doesn't load empty attributes" do
        subject.region.must_be_nil
      end

      it 'loads intpt' do
        subject.intpt.must_equal intpt
      end

      it 'loads geometry' do
        subject.geometry.must_equal geometry
      end

      describe 'across the 180th meridian' do
        let :geometry do
          factory.multi_polygon([
            factory.polygon(
              factory.linear_ring([
                factory.point( 179.5, 27.919623),
                factory.point(-179.5, 27.919623),
                factory.point(-179.5, 28.919623),
                factory.point( 179.5, 28.919623)]))])
        end

        let :intpt do
          factory.point 179.75, 28
        end

        it 'keeps geometry in the western hemisphere' do
          subject.geometry.must_equal factory.multi_polygon([
            factory.polygon(
              factory.linear_ring([
                factory.point(-180.5, 27.919623),
                factory.point(-179.5, 27.919623),
                factory.point(-179.5, 28.919623),
                factory.point(-180.5, 28.919623)]))])
        end

        it 'keeps intpt in the western hemisphere' do
          subject.intpt.must_equal factory.point(-180.25, 28)
        end
      end

      describe 'with diacritical characters' do
        let :attributes do
          {
            'aland'    => 0,
            'awater'   => 0,
            'division' => '0',
            'geoid'    => '00',
            'intptlat' => intpt.y,
            'intptlon' => intpt.x,
            'name'     => 'B\xe4z',
            'region'   => '0',
            'statens'  => '00000000',
            'stusps'   => 'AA'}
        end

        it "doesn't choke on accent marks" do
          ActiveRecord::Base.transaction do
            subject.save!
            raise ActiveRecord::Rollback
          end
        end
      end
    end
  end

  describe 'instance methods' do
    subject do
      Patchwerks::Local::Tiger::State.new
    end

    it 'returns its geoid from to_param' do
      subject.geoid = '00'
      subject.to_param.must_equal '00'
    end

    it 'is available' do
      subject.available?.must_equal true
    end
  end
end
