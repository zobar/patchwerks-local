require 'test_helper'

describe Patchwerks::Local::Tiger::State do
  subject do
    Patchwerks::Local::Tiger::State
  end

  it 'sets available on for_parent_geoid' do
    subject.for_parent_geoid(nil).available?.must_equal true
  end

  describe 'instance methods' do
    subject do
      Patchwerks::Local::Tiger::State.new
    end

    it "always has funcstat set to 'A'" do
      subject.funcstat.must_equal 'A'
    end

    it "always has lsad set to '00'" do
      subject.lsad.must_equal '00'
    end

    it "always has mtfcc set to 'G4000'" do
      subject.mtfcc.must_equal 'G4000'
    end

    it 'always has statefp equal to geoid' do
      subject.geoid = '00'
      subject.statefp.must_equal subject.geoid
    end
  end
end
