require 'test_helper'

describe Patchwerks::Local::Tiger::County do
  subject do
    Patchwerks::Local::Tiger::County
  end

  it 'can find by statefp' do
    subject.for_parent_geoid('00').where_values_hash[:statefp].must_equal '00'
  end

  describe 'instance methods' do
    subject do
      Patchwerks::Local::Tiger::County.new
    end

    it 'has mtfcc set to "G4020"' do
      subject.mtfcc.must_equal 'G4020'
    end
  end
end
