require 'test_helper'

describe Patchwerks::Local::Tiger::CensusTract do
  subject do
    Patchwerks::Local::Tiger::CensusTract
  end

  it 'can find by state geoid' do
    subject.for_parent_geoid('12').where_values_hash[:statefp].must_equal '12'
  end

  it 'can find by county geoid' do
    relation = subject.for_parent_geoid '12345'
    relation.where_values_hash[:statefp].must_equal '12'
    relation.where_values_hash[:countyfp].must_equal '345'
  end

  describe 'instance methods' do
    subject do
      Patchwerks::Local::Tiger::CensusTract.new
    end

    it 'has funcstat set to "S"' do
      subject.funcstat.must_equal 'S'
    end

    it 'has mtfcc set to "G5020"' do
      subject.mtfcc.must_equal 'G5020'
    end
  end
end
