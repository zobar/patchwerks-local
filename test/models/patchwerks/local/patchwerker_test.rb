require 'test_helper'

describe Patchwerks::Local::Patchwerker do

  describe 'queues' do
    before do
      EventMachine.expects(:run).yields
    end

    subject do
      Patchwerks::Local::Patchwerker.new(
          [], 'rails-root' => Rails.root).tap do |subject|
        subject.expects(:amqp_queue).
            with('Patchwerks::Local::TigerFile#dequeue').
            returns(queue)
      end
    end

    let :message do
      mock
    end

    let :queue do
      mock.tap do |queue|
        queue.expects(:subscribe).with(ack: true).yields(message, 1)
      end
    end

    let :tiger_file do
      mock.tap do |tiger_file|
        tiger_file.stubs(:upstream_file).returns('foo')
      end
    end

    it 'acks on ActiveRecord::RecordNotFound' do
      Patchwerks::Local::TigerFile.expects(:find).raises(
          ActiveRecord::RecordNotFound)
      message.expects(:ack)
      begin
        subject.queues
      rescue ActiveRecord::RecordNotFound
      end
    end

    describe 'with a valid tiger file ID' do
      before do
        Patchwerks::Local::TigerFile.expects(:find).with(1).returns(tiger_file)
      end

      it 'can process queue items' do
        message.expects(:ack)
        tiger_file.expects(:dequeue)
        subject.queues
      end

      it 'does not ack on error' do
        message.expects(:ack).never
        tiger_file.expects(:dequeue).raises
        begin
          subject.queues
        rescue
        end
      end
    end
  end

end
