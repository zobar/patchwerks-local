require 'test_helper'

describe Patchwerks::Local::Pending::CountyCollection do
  subject do
    Patchwerks::Local::Pending::CountyCollection.new
  end

  it 'has the correct upstream file name' do
    subject.upstream_file.must_equal 'COUNTY/tl_2012_us_county'
  end
end
