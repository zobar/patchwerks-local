require 'test_helper'

describe Patchwerks::Local::Pending::CensusTract do
  subject do
    Patchwerks::Local::Pending::CensusTract.new '12345678901'
  end

  it 'has the correct upstream file name' do
    subject.upstream_file.must_equal 'TRACT/tl_2012_12_tract'
  end
end
