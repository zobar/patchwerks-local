require 'test_helper'

describe Patchwerks::Local::Pending::State do
  subject do
    Patchwerks::Local::Pending::State.new '00'
  end

  it 'has the correct upstream file name' do
    subject.upstream_file.must_equal 'STATE/tl_2012_us_state'
  end
end
