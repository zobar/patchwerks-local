require 'test_helper'

describe Patchwerks::Local::Pending::County do
  subject do
    Patchwerks::Local::Pending::County.new '00000'
  end

  it 'has the correct upstream file name' do
    subject.upstream_file.must_equal 'COUNTY/tl_2012_us_county'
  end
end
