require 'test_helper'

describe Patchwerks::Local::Pending::Model do
  subject do
    Patchwerks::Local::Pending::State.new '00'
  end

  it 'knows its geoid' do
    subject.geoid.must_equal '00'
  end

  it 'is not available' do
    subject.available?.must_equal false
  end

  it 'can enqueue itself' do
    subject.tiger_file.expects(:save!)
    subject.enqueue
  end

  it 'can retrieve its TigerFile' do
    subject.tiger_file.must_be_kind_of Patchwerks::Local::TigerFile
  end
end
