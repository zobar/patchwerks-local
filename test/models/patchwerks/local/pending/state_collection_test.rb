require 'test_helper'

describe Patchwerks::Local::Pending::StateCollection do
  subject do
    Patchwerks::Local::Pending::StateCollection.new
  end

  it 'has the correct upstream file name' do
    subject.upstream_file.must_equal 'STATE/tl_2012_us_state'
  end
end
