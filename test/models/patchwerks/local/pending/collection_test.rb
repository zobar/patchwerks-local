require 'test_helper'

describe Patchwerks::Local::Pending::Collection do
  subject do
    Patchwerks::Local::Pending::StateCollection.new
  end

  it 'knows its class name' do
    subject.class_name.must_equal 'State'
  end

  it 'has a dummy implementation of order' do
    subject.order(:geoid).must_be_kind_of Patchwerks::Local::Pending::Collection
  end

  it 'has a dummy implementation of with_geometry' do
    subject.with_geometry.must_be_kind_of Patchwerks::Local::Pending::Collection
  end
end
