require 'test_helper'

describe Patchwerks::Local::Pending::CensusTractCollection do
  subject do
    Patchwerks::Local::Pending::CensusTractCollection.new '12345'
  end

  it 'has the correct upstream file name' do
    subject.upstream_file.must_equal 'TRACT/tl_2012_12_tract'
  end
end
