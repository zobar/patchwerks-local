require 'test_helper'

describe Patchwerks::Local::TigerFile do
  subject do
    Patchwerks::Local::TigerFile.for_pending(pending).build
  end

  let :pending do
    Patchwerks::Local::Pending::State.new '00'
  end

  it "can be created with a pending object's class name" do
    subject.class_name.must_equal 'State'
  end

  it "can be created with a pending object's upstream file" do
    subject.upstream_file.must_equal pending.upstream_file
  end

  it 'can load a TIGER/Line file' do
    record = RGeo::Shapefile::Reader::Record.new(0, nil, {})
    temp_dir = File.join(Rails.root, 'tmp', 'cache', 'patchwerks', 'tiger',
                         'STATE')
    shp_path = File.join(temp_dir, 'tl_2012_us_state')
    tiger_object = mock
    zip_path = File.join(temp_dir, 'tl_2012_us_state.zip')
    subject.expects(:cached).with(
        "http://www2.census.gov/geo/tiger/TIGER2012/STATE/tl_2012_us_state.zip",
        'STATE/tl_2012_us_state.zip').yields(zip_path)
    subject.expects(:enqueue_direct).never
    tiger_object.expects(:save!)
    File.stubs(:exists?).returns(true)
    Patchwerks::Local::Tiger::State.expects(:for_record).with(record).returns(
        tiger_object)
    RGeo::Shapefile::Reader.expects(:open).with(shp_path, srid: 4326).yields(
        [record])
    Zip::ZipFile.expects(:open).with(zip_path).yields(nil)
    subject.dequeue
  end

  it 'enqueues itself on save if not loaded' do
    subject.expects(:enqueue_direct)
    subject.save!
  end

  it 'does not enqueue itself on save if loaded' do
    subject.expects(:enqueue_direct).never
    subject.loaded = true
    subject.save!
  end

  it "is not loaded when it's first created" do
    subject.valid?
    subject.loaded.must_equal false
  end
end
