require 'test_helper'

describe Patchwerks::Local::Cache do
  after do
    Excon.stubs.clear
    Fog::Mock.reset
  end

  let :local_path do
    File.join subject.local_cache_directory, subject.cache_prefix, 'foo', 'bar'
  end

  subject do
    Object.new.tap do |subject|
      class << subject
        include Patchwerks::Local::Cache
      end
    end
  end

  it 'returns local files immediately' do
    subject.intermediate_cache
    File.expects(:exists?).with(local_path).returns(true)
    File.expects(:open).with(local_path).returns('foo')

    subject.cached 'http://example.com', 'foo/bar' do |path|
      path.must_equal local_path
    end
  end

  it 'returns files from the intermediate cache if necessary' do
    file = mock
    file.expects(:write).with('foo')
    subject.intermediate_cache.create body: 'foo',
                                      key: "#{subject.cache_prefix}foo/bar"
    File.expects(:exists?).with(local_path).returns(false)
    File.expects(:open).with(local_path, 'wb').yields(file)
    FileUtils.expects(:mkdir_p).with(File.dirname(local_path))
    subject.cached('http://example.com', 'foo/bar') {|path|}
  end

  it 'returns files from the origin if necessary' do
    file = mock
    file.expects(:write).with('foo')
    subject.intermediate_cache
    Excon.stub({host: 'example.com', method: :get}, {body: 'foo'})
    File.expects(:exists?).with(local_path).returns(false)
    File.expects(:open).with(local_path).yields('foo')
    File.expects(:open).with(local_path, 'wb').yields(file)
    FileUtils.expects(:mkdir_p).with(File.dirname(local_path))

    subject.cached('http://example.com', 'foo/bar') {|path|}
  end

  it 'cleans up cache file on fetch error' do
    subject.intermediate_cache
    File.expects(:delete).with(local_path)
    File.stubs(:exists?).with(local_path).returns(false).then.returns(true)
    File.expects(:open).with(local_path, 'wb').raises(StandardError)
    FileUtils.expects(:mkdir_p).with(File.dirname(local_path))
    begin
      subject.cached('http://example.com', 'foo/bar') {|path|}
    rescue
    end
  end

  it 'has an intermediate cache' do
    subject.intermediate_cache.must_respond_to :create
    subject.intermediate_cache.must_respond_to :get
    subject.intermediate_cache.must_respond_to :head
  end

  it 'puts local files in tmp/cache/patchwerks' do
    subject.local_cache_directory.must_equal File.join(Rails.root, 'tmp',
                                                       'cache', 'patchwerks')
  end
end
