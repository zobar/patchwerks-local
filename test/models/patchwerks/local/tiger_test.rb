require 'test_helper'

describe Patchwerks::Local::Tiger do

  subject do
    Patchwerks::Local::Tiger
  end

  it 'namespaces tables' do
    subject.table_name_prefix.must_equal 'patchwerks_local_tiger_'
  end

end
