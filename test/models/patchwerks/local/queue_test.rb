require 'test_helper'

describe Patchwerks::Local::Queue do
  subject do
    Object.new.tap do |subject|
      class << subject
        include Patchwerks::Local::Queue
      end
    end
  end

  it 'gives the same AMQP channel for the same thread' do
    subject.stubs(:amqp_connection)
    AMQP::Channel.stubs(:new).returns(1).then.returns(2)
    queue_1 = subject.amqp_channel
    queue_2 = subject.amqp_channel
    queue_1.must_equal queue_2
    Thread.current[:amqp_channel] = nil
  end

  it 'gives different AMQP channels for different threads' do
    subject.stubs(:amqp_connection)
    AMQP::Channel.stubs(:new).returns(1).then.returns(2)
    queue_1 = subject.amqp_channel
    queue_2 = queue_1
    thread = Thread.new do
      queue_2 = subject.amqp_channel
    end
    thread.join
    queue_1.wont_equal queue_2
    Thread.current[:amqp_channel] = nil
  end

  it 'starts AMQP in EventMachine if available' do
    AMQP.expects(:start)
    EventMachine.expects(:reactor_running?).returns(true)
    subject.amqp_connection
  end

  it "starts AMQP in a separate thread if EventMachine isn't running" do
    AMQP.expects(:start).yields
    EventMachine.expects(:reactor_running?).returns(false)
    subject.amqp_connection
  end

  it 'prints an error if AMQP raises an exception from a separate thread' do
    subject.expects(:puts)
    AMQP.expects(:start).raises
    EventMachine.expects(:reactor_running?).returns(false)
    subject.amqp_connection
  end

  it 'provides default exchange options' do
    subject.amqp_exchange_options('foo').must_equal({})
  end

  it 'allows override of exchange options' do
    class << subject
      def amqp_exchange_options(name)
        {foo: :bar}
      end
    end
    subject.amqp_exchange_options('foo').must_equal({foo: :bar})
  end

  it 'provides default mesage options' do
    subject.amqp_message_options('foo', 'bar').must_equal({})
  end

  it 'allows override of message options' do
    class << subject
      def amqp_message_options(name, data)
        {foo: :bar}
      end
    end
    subject.amqp_message_options('foo', 'bar').must_equal({foo: :bar})
  end

  it 'provides default queue options' do
    subject.amqp_queue_options('foo').must_equal({})
  end

  it 'allows override of queue options' do
    class << subject
      def amqp_queue_options(name)
        {foo: :bar}
      end
    end
    subject.amqp_queue_options('foo').must_equal({foo: :bar})
  end

  describe 'channel operations' do
    subject do
      Object.new.tap do |subject|
        class << subject
          include Mocha::API
          include Patchwerks::Local::Queue

          def amqp_channel
            @amqp_channel ||= mock
          end
        end
      end
    end

    it 'creates a direct exchange and queue' do
      subject.amqp_channel.expects(:direct).with(
          '', subject.amqp_exchange_options('foo').merge(key: 'foo'))
      subject.amqp_channel.expects(:queue).with(
          'foo', subject.amqp_queue_options('foo'))
      subject.amqp_direct_exchange 'foo'
    end

    it 'creates a queue' do
      subject.amqp_channel.expects(:queue).with(
          'foo', subject.amqp_queue_options('foo'))
      subject.amqp_queue 'foo'
    end

    it 'can enqueue a direct message' do
      class << subject
        def amqp_direct_exchange(name)
          @amqp_direct_exchange ||= mock
        end
      end

      subject.amqp_direct_exchange('foo').expects(:publish).with(
          'bar', subject.amqp_message_options('foo', 'bar'))
      subject.enqueue_direct 'foo', 'bar'
    end
  end
end
