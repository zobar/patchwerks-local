require 'test_helper'

describe Patchwerks::Local::Factory do
  let :factory do
    RGeo::Cartesian.preferred_factory srid: 4326
  end

  let :shape do
    Patchwerks::Local::Shape.new.tap do |shape|
      shape.geometry = factory.multi_polygon [
        factory.polygon(
          factory.linear_ring([
            factory.point(0, 0),
            factory.point(1, 0),
            factory.point(1, 1),
            factory.point(0, 1)]))]
      shape.intpt = factory.point(0, 0)
    end
  end

  let :state do
    Patchwerks::Local::Tiger::State.new.tap do |state|
      {
        aland:    0,
        awater:   0,
        division: '0',
        geoid:    '00',
        name:     'Foo',
        region:   '0',
        shape:    shape,
        statens:  '00000000',
        stusps:   'AA'}.each {|k, v| state.send :"#{k}=", v}
    end
  end

  subject do
    Patchwerks::Local::State
  end

  it 'responds to for_parent_geoid with a Mock object when mocking' do
    subject.mock do
      subject.for_parent_geoid.must_be_kind_of(
          Patchwerks::Local::Mock::Collection)
    end
  end

  it 'responds to for_parent_geoid with AR relation when available' do
    ActiveRecord::Base.transaction do
      state.save!
      subject.for_parent_geoid.must_be_kind_of ActiveRecord::Relation
      raise ActiveRecord::Rollback
    end
  end

  it 'responds to for_parent_geoid with Pending collection when not available' do
    subject.for_parent_geoid.must_be_kind_of(
        Patchwerks::Local::Pending::Collection)
  end

  it 'responds to for_geoid with a Mock object when mocking' do
    subject.mock do
      subject.for_geoid('00').must_be_kind_of Patchwerks::Local::Mock::Model
    end
  end

  it 'responds to for_geoid with Tiger model when available' do
    ActiveRecord::Base.transaction do
      state.save!
      subject.for_geoid('00').must_be_kind_of Patchwerks::Local::Tiger::Model
      raise ActiveRecord::Rollback
    end
  end

  it 'responds to for_geoid with Pending model when not available' do
    subject.for_geoid('00').must_be_kind_of Patchwerks::Local::Pending::Model
  end

  it 'returns false for mocking? when not mocking' do
    subject.mocking?.must_equal false
  end

  it 'returns true for mocking? when mocking' do
    subject.mock do
      subject.mocking?.must_equal true
    end
  end

  it 'memoizes pending_collection_class' do
    pending_collection_class_1 = subject.pending_collection_class
    pending_collection_class_2 = subject.pending_collection_class
    pending_collection_class_1.must_equal pending_collection_class_2
  end
end
