require 'test_helper'

describe Patchwerks::Local::Mock::Model do
  subject do
    Patchwerks::Local::State.for_geoid '00'
  end

  it 'has a geometry' do
    Patchwerks::Local::State.mock do
      subject.shape.must_be_kind_of Patchwerks::Local::Mock::Shape
    end
  end
end
