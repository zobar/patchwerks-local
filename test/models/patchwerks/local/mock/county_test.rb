require 'test_helper'

describe Patchwerks::Local::Mock::County do
  subject do
    Patchwerks::Local::County.for_geoid '00000'
  end

  it 'has a name' do
    Patchwerks::Local::County.mock do
      subject.name.must_be_kind_of String
    end
  end
end
