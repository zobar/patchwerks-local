require 'test_helper'

describe Patchwerks::Local::Mock::Shape do
  subject do
    Patchwerks::Local::State.for_geoid('00').shape
  end

  it 'has geometry' do
    Patchwerks::Local::State.mock do
      RGeo::Feature::MultiPolygon.must_be :check_type, subject.geometry
    end
  end
end
