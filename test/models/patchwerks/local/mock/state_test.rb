require 'test_helper'

describe Patchwerks::Local::Mock::State do
  subject do
    Patchwerks::Local::State.for_geoid '00'
  end

  it 'has a name' do
    Patchwerks::Local::State.mock do
      subject.name.must_be_kind_of String
    end
  end
end
