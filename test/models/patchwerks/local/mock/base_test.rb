require 'test_helper'

describe Patchwerks::Local::Mock::Base do
  subject do
    Patchwerks::Local::State.for_geoid '00'
  end

  it 'returns true for available? when mocking Tiger data' do
    Patchwerks::Local::State.mock do
      subject.available?.must_equal true
    end
  end

  it 'returns false for available? when mocking Pending data' do
    Patchwerks::Local::State.mock :pending do
      subject.available?.must_equal false
    end
  end

  it 'returns nil when mocking not found data' do
    Patchwerks::Local::State.mock :not_found do
      subject.must_be :nil?
    end
  end
end
