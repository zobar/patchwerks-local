require 'test_helper'

describe Patchwerks::Local::Mock::Collection do
  subject do
    Patchwerks::Local::State.all
  end

  it 'has a count' do
    Patchwerks::Local::State.mock do
      subject.count.must_be_kind_of Fixnum
    end
  end

  it 'is enumerable' do
    Patchwerks::Local::State.mock do
      subject.each do |state|
        state.must_be_kind_of Patchwerks::Local::Mock::State
        break
      end
    end
  end

  it 'responds to order' do
    Patchwerks::Local::State.mock do
      subject.order.must_be_kind_of Patchwerks::Local::Mock::Collection
    end
  end

  it 'responds to with_geometry' do
    Patchwerks::Local::State.mock do
      subject.with_geometry.must_be_kind_of Patchwerks::Local::Mock::Collection
    end
  end
end
