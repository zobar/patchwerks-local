require 'test_helper'

describe Patchwerks::Local::Mock::CensusTract do
  subject do
    Patchwerks::Local::CensusTract.for_geoid '00000000000'
  end

  it 'has a name' do
    Patchwerks::Local::CensusTract.mock do
      subject.name.must_be_kind_of String
    end
  end
end
