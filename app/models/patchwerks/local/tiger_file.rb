#--
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

require 'rgeo/shapefile'
require 'zip/zip'

module Patchwerks::Local

  #
  # Represents a TIGER dataset that may or may not be loaded in the database.
  #
  class TigerFile < Model
    include Cache
    include Queue

    before_validation :update_loaded
    after_create      :enqueue

    #
    # Retrieve the TigerFile that should contain a Pending object, if it already
    # exists in the database. (This can happen if another object in the same
    # data set has already enqueued the data set.)
    #
    scope :for_pending, ->(pending) do
      where class_name:    pending.class_name,
            upstream_file: pending.upstream_file
    end

    #
    # Fetch a TigerFile and import it into the database. This is called from the
    # +patchwerker+.
    #
    def dequeue
      ActiveRecord::Base.transaction do
        cached upstream_url, "#{upstream_file}.zip" do |zip_path|
          base_name = File.basename zip_path, '.zip'
          zip_dir = File.dirname zip_path
          Zip::ZipFile.open zip_path do |zip|
            %w[dbf shp shx].each do |extension|
              file_name = "#{base_name}.#{extension}"
              file_path = File.join(zip_dir, file_name)
              zip.extract file_name, file_path unless File.exists? file_path
            end
          end

          RGeo::Shapefile::Reader.open(File.join(zip_dir, base_name),
                                       srid: 4326) do |shp|
            shp.each do |record|
              tiger = factory.for_record record
              tiger.save!
            end
          end
        end

        self.loaded = true
        save!
      end
    end

    protected

      #
      # Queue up a TigerFile to be imported into the database.
      #
      def enqueue
        enqueue_direct 'Patchwerks::Local::TigerFile#dequeue', id unless loaded
      end

      #
      # Returns the factory module that should be used to create objects
      # contained in this TigerFile.
      #
      def factory
        return @factory if instance_variable_defined? :@factory
        @factory = Patchwerks::Local.const_get class_name
      end

      #
      # Ensure that +loaded+ is not nil.
      #
      def update_loaded
        self.loaded = false if loaded.nil?
        true
      end

      #
      # Base URL for retrieving TIGER data sets.
      #
      def upstream_site
        'http://www2.census.gov/geo/tiger/TIGER2012'
      end

      #
      # Full URL for this TigerFile's data set.
      #
      def upstream_url
        "#{upstream_site}/#{upstream_file}.zip"
      end
  end
end
