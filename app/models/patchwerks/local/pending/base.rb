#--
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

module Patchwerks::Local

  #
  # Implements common functionality required by all Pending objects.
  #
  class Pending::Base

    #
    # Pending objects are not available in the database.
    #
    def available?
      false
    end

    #
    # Returns the class name of objects represented by this class. The Pending
    # class will have this name in the Patchwerks::Local::Pending namespace; the
    # TIGER class will have this name in the Patchwerks::Local::Tiger namespace.
    #
    def class_name
      return @class_name if instance_variable_defined? :@class_name
      @class_name = self.class.name.demodulize
    end

    #
    # Enqueues a job to load the TIGER data set that should contain this object.
    # This will not add a job to the queue if one already exists.
    #
    def enqueue
      tiger_file.save!
      tiger_file
    end

    #
    # Returns the TigerFile representing the data set that should contain this
    # object.
    #
    def tiger_file
      return @tiger_file if instance_variable_defined? :@tiger_file
      @tiger_file = TigerFile.for_pending(self).first_or_initialize
    end
  end
end
