#--
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

module Patchwerks::Local::Pending

  #
  # Represents a collection of related objects that have yet to be loaded (for
  # example, all the counties of a state). Subclasses should be in the
  # Patchwerks::Local::Pending namespace following the naming convention of
  # +{{Model}}Collection+.
  #
  class Collection < Base

    #
    # The +geoid+ of the parent record. If the objects don't have a parent, or
    # the parent isn't relevant, +parent_geoid+ may be +nil+.
    #
    attr_reader :parent_geoid

    #
    # Returns the class name of objects represented by this class. The Pending
    # class will have this name in the Patchwerks::Local::Pending namespace; the
    # TIGER class will have this name in the Patchwerks::Local::Tiger namespace.
    #
    def class_name
      @class_name ||= super.sub /Collection\Z/, ''
    end

    #
    # Creates a new pending collection. This should only be called from the
    # factories in Patchwerks::Local.
    #
    # [parent_geoid] The +geoid+ of the objects' parent. If the objects do not
    #                have a parent or the parent is irrelevant, this may be
    #                +nil+.
    #
    def initialize(parent_geoid=nil)
      @parent_geoid = parent_geoid
    end

    #
    # Orders the collection. Since there are never any items in a Pending
    # Collection, this method is a no-op.
    #
    def order(*args)
      self
    end

    #
    # Hint that the collection's geometry should be loaded along with the data.
    # Since there are never any items in a Pending Collection, this method is a
    # no-op.
    #
    def with_geometry
      self
    end
  end
end
