#--
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

module Patchwerks::Local::Pending

  #
  # CensusTract is a statistical subdivision of Counties in the TIGER hierarchy.
  #
  class CensusTract < Model

    #
    # The +statefp+ for a Pending CensusTract is equal to the first two
    # characters of its +geoid+.
    #
    def statefp
      @statefp ||= geoid[0..1]
    end

    #
    # CensusTracts can be found in state-based files with names of the format
    # +TRACT/tl_2012_{{statefp}}_tract+.
    #
    def upstream_file
      "TRACT/tl_2012_#{statefp}_tract"
    end
  end
end
