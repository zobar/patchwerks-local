#--
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

module Patchwerks::Local

  #
  # Base class for loaded TIGER objects. Subclasses must implement +to_param+,
  # which returns a unique identifier for the object, and +find_by_param+, which
  # returns a Relation of the record with a unique identifier.
  #
  class Tiger::Model < Model
    self.abstract_class = true

    include Geometric

    #
    # In order to simplify geometric calculations, coordinates east of
    # WRAP_LONGITUTE will be loaded into the database as west of 180 degrees
    # west. This means that a longitude of 179 degrees east will instead be
    # loaded as 181 degrees west. The choice of longitute is arbitrary;
    # 40.025376 was chosen as being diametrically opposite of the geometric
    # center of the TIGER/Line dataset.
    #
    WRAP_LONGITUDE = 40.025376

    scope :collection, scoped.extending(Tiger::Collection)

    #
    # Returns a Relation of the model with the given +geoid+.
    #
    # [geoid] +geoid+ for the requested object.
    #
    def self.for_geoid(geoid)
      where(geoid: geoid)
    end

    #
    # Finds or initializes a Tiger object with data from a shapefile record.
    # Attributes are updated with the values in the record. The record is not
    # saved.
    #
    # [record] An RGeo::Shapefile::Reader::Record containing values for the
    #          object.
    #
    def self.for_record(record)
      attributes = {}.with_indifferent_access
      record.attributes.each {|k, v| attributes[k.downcase] = v}
      columns = Set.new column_names
      factory = record.geometry.factory
      model = self.for_geoid(attributes[:geoid]).includes(:geometry).
          first_or_initialize

      attributes.each do |key, value|
        setter = :"#{key}="
        if model.respond_to? setter
          value = nil if value.blank?
          if value.is_a? String
            value = value.force_encoding('ISO-8859-1').encode 'UTF-8'
          end
          model.send setter, value
        end
      end

      shape = model.shape || model.build_shape
      if record.geometry.envelope.exterior_ring.point_n(2).x > WRAP_LONGITUDE
        shape.geometry = wrap_geometry(record.geometry)
      else
        shape.geometry = record.geometry
      end
      if attributes.has_key?(:intptlat) && attributes.has_key?(:intptlon)
        shape.intpt = wrap_geometry factory.point(attributes[:intptlon].to_f,
                                                  attributes[:intptlat].to_f)
      end

      model
    end

    #
    # TIGER objects are available in the local database.
    #
    def available?
      true
    end

    #
    # Returns the unique identifier for an object, which is the +geoid+.
    #
    def to_param
      geoid
    end

    protected

      #
      # Creates an equivalent RGeo::Feature with coordinates east of
      # WRAP_LONGITUDE replaced with equivalent coordinates west of
      # WRAP_LONGITUDE. You will not need to call this function on a feature
      # unless the max_x of its bounding box is greater than WRAP_LONGITUDE.
      #
      # [geometry] The geometry to wrap.
      #
      def self.wrap_geometry(geometry)
        factory = geometry.factory
        recurse = method __method__
        case geometry
        when RGeo::Feature::LinearRing
          factory.linear_ring geometry.points.collect(&recurse)
        when RGeo::Feature::MultiPolygon
          factory.multi_polygon geometry.collect(&recurse)
        when RGeo::Feature::Point
          if geometry.x > WRAP_LONGITUDE
            factory.point geometry.x - 360, geometry.y
          else
            geometry
          end
        when RGeo::Feature::Polygon
          factory.polygon wrap_geometry(geometry.exterior_ring),
                          geometry.interior_rings.collect(&recurse)
        end
      end
  end
end
