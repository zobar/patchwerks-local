#--
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

module Patchwerks::Local::Tiger

  #
  # State is the top level of the TIGER hierarchy. States have the following
  # attributes (see Section 5.18.1 of the {2012 TIGER/Line Shapefiles Technical
  # Documentation}[http://www.census.gov/geo/www/tiger/tgrshp2012/documentation.html]).
  #
  #   region   varchar(2)   Current region code
  #   division varchar(2)   Current division code
  #   statefp  char(2)      Current state FIPS code
  #   statens  char(8)      Current state ANSI code
  #   geoid    char(2)      State identifier; state FIPS code
  #   stusps   char(2)      Current United States Postal Service state
  #                         abbreviation
  #   name     varchar(100) Current state name
  #   lsad     char(2)      Current legal/statistical area description code for
  #                         state
  #   mtfcc    char(5)      MAF/TIGER feature class code (G4000)
  #   funcstat char(1)      Current functional status
  #   aland    bigint       Current land area
  #   awater   bigint       Current water area
  #
  class State < Model

    scope :for_parent_geoid, ->(parent_geoid) do
      collection
    end

    #
    # +funcstat+ is always <tt>'A'</tt>.
    #
    def funcstat
      'A'
    end

    #
    # +lsad+ is always <tt>'00'</tt>.
    #
    def lsad
      '00'
    end

    #
    # +mtfcc+ is always <tt>'G4000'</tt>.
    #
    def mtfcc
      'G4000'
    end

    #
    # +statefp+ is always equal to +geoid+.
    #
    def statefp
      geoid
    end
  end
end
