#--
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

module Patchwerks::Local::Tiger

  #
  # County is the primary division of States in the TIGER hierarchy. Counties
  # have the following attributes (see Section 5.7.1 of the {2012 TIGER/Line
  # Shapefiles Technical
  # Documentation}[http://www.census.gov/geo/www/tiger/tgrshp2012/documentation.html]).
  #
  #   statefp  char(2)      Current state FIPS code
  #   countyfp char(3)      Current county FIPS code
  #   countyns char(8)      Current county ANSI code
  #   geoid    char(5)      County identifier; a concatenation of Current state
  #                         FIPS code and county FIPS code
  #   name     varchar(100) Current county name
  #   namelsad varchar(100) Current name and the translated legal/statistical
  #                         area description for county
  #   lsad     char(2)      Current legal/statistical area description code for
  #                         county
  #   classfp  char(2)      Current FIPS class code
  #   mtfcc    char(5)      MAF/TIGER feature class code (G4020)
  #   csafp    char(3)      Current combined statistical area code
  #   cbsafp   char(5)      Current metropolitan statistical area/micropolitan
  #                         statistical area code
  #   metdivfp char(5)      Current metropolitan division code
  #   funcstat char(1)      Current functional status
  #   aland    bigint       Current land area
  #   awater   bigint       Current water area
  #
  class County < Model
    scope :for_parent_geoid, ->(parent_geoid) do
      result = collection
      result = result.where(statefp: parent_geoid) unless parent_geoid.nil?
      result
    end

    #
    # The MTFCC is always "G4020".
    #
    def mtfcc
      'G4020'
    end
  end
end
