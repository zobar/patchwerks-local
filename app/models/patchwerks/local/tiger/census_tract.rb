#--
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

module Patchwerks::Local::Tiger

  #
  # CensusTract is a statistical subdivision of Counties in the TIGER hierarchy.
  # CensusTracts have the following attributes (see Section 5.4.1 of the {2012
  # TIGER/Line Shapefiles Technical
  # Documentation}[http://www.census.gov/geo/www/tiger/tgrshp2012/documentation.html]).
  #
  #   statefp  char(2)  Current state FIPS code
  #   countyfp char(3)  Current county FIPS code
  #   tractce  char(6)  Current census tract code
  #   geoid    char(5)  County identifier; a concatenation of Current state
  #                     FIPS, county FIPS code, and census tract code
  #   name     char(7)  Current census tract name, this is the census tract code
  #                     converted to an integer or integer plus two-digit
  #                     decimal if the last two characters of the code are not
  #                     both zeros.
  #   namelsad char(20) Current translated legal/statistical area description
  #                     and the census tract name
  #   mtfcc    char(5)  MAF/TIGER feature class code (G5020)
  #   funcstat char(1)  Current functional status
  #   aland    bigint   Current land area
  #   awater   bigint   Current water area
  #
  class CensusTract < Model
    scope :for_parent_geoid, ->(parent_geoid) do
      result = collection
      if parent_geoid.length >= 2
        result = result.where(statefp: parent_geoid[0..1])
      end
      if parent_geoid.length >= 5
        result = result.where(countyfp: parent_geoid[2..4])
      end
      result
    end

    #
    # +funcstat+ is always 'S'
    #
    def funcstat
      'S'
    end

    #
    # +mtfcc+ is always 'G5020'
    #
    def mtfcc
      'G5020'
    end
  end
end
