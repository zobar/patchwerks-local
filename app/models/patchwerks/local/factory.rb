#--
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

module Patchwerks::Local

  #
  # This module defines the common interface for retrieving geographic objects.
  # It is intended to be extended by modules that provide access to specific
  # geographic types, like State or County.
  #
  module Factory
    delegate :for_record, to: :tiger_class

    #
    # Returns a collection of objects with a particular parent. If there are any
    # objects in the database with that parent, an ActiveRecord::Relation is
    # returned (it will respond to +available?+ with +true+). If there are no
    # matching objects in the database but the TIGER dataset they belong to has
    # not yet been loaded, this will return a
    # Patchwerks::Local::Tiger::Collection subclass (+available?+ returns
    # +false+). If there are no matching objects in the database and the TIGER
    # dataset they would have belonged to _has_ been loaded, this method returns
    # +nil+.
    #
    # [parent_geoid] +geoid+ of the objects' parent. If the objects do not have
    #                a parent or the parent is not relevant, this may be +nil+.
    #
    def for_parent_geoid(parent_geoid=nil)
      if mocking?
        return mock_collection_class.mock(@mock_behavior, parent_geoid)
      end
      result = tiger_class.for_parent_geoid(parent_geoid)
      if result.empty?
        result = pending_collection_class.new parent_geoid
        result = nil if result.tiger_file.try :loaded
      end
      result
    end

    #
    # Retrieve a geographic object by ID. If the object exists in the database,
    # this will return a Patchwerks::Local::Tiger::Model subclass (+available?+
    # returns +true+). If the object does not exist in the database and the
    # TIGER dataset it belongs to has not yet been loaded, this will return a
    # Patchwerks::Local::Pending::Model subclass (+available?+ returns +false+)
    # that can be used to enqueue a load request. If the object does not exist
    # in the database but the TIGER dataset it would have belonged to _has_ been
    # loaded, this function returns +nil+.
    #
    # [geoid] The +geoid+ of the object to retrieve. This will be a String.
    #
    def for_geoid(geoid)
      return mock_class.mock(@mock_behavior, geoid) if mocking?
      return nil if geoid.nil?
      result = tiger_class.for_geoid(geoid).first
      if result.nil?
        result = pending_class.new geoid
        result = nil if result.tiger_file.try :loaded
      end
      result
    end

    #
    # Enable mocking for a specific Patchwerks type. Within the context of this
    # call, all objects of a particular type will be mocked.
    #
    #   Patchwerks::Local::State.mock :pending do
    #     # test pending States
    #   end
    #
    # [mock_behavior] Determines the behavior of mocked objects. One of
    #                 +:not_found+, +:pending+, or +:tiger+.
    #
    def mock(mock_behavior=:tiger)
      old_mock_behavior = @mock_behavior
      begin
        @mock_behavior = mock_behavior
        yield
      ensure
        @mock_behavior = old_mock_behavior
      end
    end

    #
    # Returns the class of mocked objects.
    #
    def mock_class
      return @mock_class if instance_variable_defined? :@mock_class
      @mock_class = Mock.const_get name.demodulize
    end

    #
    # Returns the class of mocked collections.
    #
    def mock_collection_class
      if instance_variable_defined? :@mock_collection_class
        return @mock_collection_class
      end
      @mock_collection_class = Mock.const_get "#{name.demodulize}Collection"
    end

    #
    # Returns true if the factory is currently mocking.
    #
    def mocking?
      !@mock_behavior.nil?
    end

    #
    # Returns the class of Pending objects created by this Factory.
    #
    def pending_class
      return @pending_class if instance_variable_defined? :@pending_class
      @pending_class = Pending.const_get name.demodulize
    end

    #
    # Returns the class of Pending collections created by this Factory.
    #
    def pending_collection_class
      if instance_variable_defined? :@pending_collection_class
        return @pending_collection_class
      end
      @pending_collection_class = Pending.const_get(
          "#{name.demodulize}Collection")
    end

    #
    # Returns the class of Tiger objects created by this Factory.
    #
    def tiger_class
      return @tiger_class if instance_variable_defined? :@tiger_class
      @tiger_class = Tiger.const_get name.demodulize
    end
  end
end
