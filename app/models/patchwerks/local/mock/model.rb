#--
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

module Patchwerks::Local::Mock

  #
  # Base class for mock models.
  #
  class Model < Base
    delegate :geometry, :geometry=, :intpt, to: :shape

    #
    # The mock object's +geoid+.
    #
    attr_reader :geoid

    #
    # Return a mock Shape for the object.
    #
    def shape
      return @shape if instance_variable_defined? :@shape
      Shape.new
    end

    #
    # Create a new mock model.
    #
    # [mock_behavior] Determines the behavior of the mock object. One of
    #                 +:pending+ or +:tiger+.
    # [geoid]         Sets the +geoid+ of the mock object.
    #
    def initialize(mock_behavior, geoid)
      super mock_behavior
      @geoid = geoid
    end
  end
end
