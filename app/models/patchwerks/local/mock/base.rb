#--
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

#
# Base class for all mock objects.
#
class Patchwerks::Local::Mock::Base

  #
  # Determines the object's mock behavior. One of +:pending+ or +:tiger+.
  #
  attr_reader :mock_behavior

  #
  # Create a mock object with a specific behavior.
  #
  # [mock_behavior] Determines the behavior of the mock object. One of
  #                 +:not_found+, :+pending+, or +:tiger+.
  # [args]          Additional arguments to pass to the constructor.
  #
  def self.mock(mock_behavior, *args)
    if mock_behavior != :not_found
      new mock_behavior, *args
    end
  end

  #
  # Returns true if mocking an available object; false otherwise.
  #
  def available?
    mock_behavior == :tiger
  end

  #
  # Pretends to enqueue the object.
  #
  def enqueue
  end

  #
  # Creates a mock object with a specific behavior. This should only be called
  # by +mock+.
  #
  # [mock_behavior] Determines the behavior of the mock object. One of
  #                 :+pending+ or +:tiger+.
  #
  def initialize(mock_behavior)
    @mock_behavior = mock_behavior
  end
end
