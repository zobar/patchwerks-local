#--
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

module Patchwerks::Local

  #
  # Provides a mock Geometry object.
  #
  class Mock::Shape

    #
    # Allows you to set the object's geometry.
    #
    attr_writer :geometry

    #
    # Provides default geometry for the object.
    #
    def geometry
      return @geometry if instance_variable_defined? :@geometry
      factory = RGeo::Cartesian.preferred_factory srid: 4326
      @geometry = factory.multi_polygon([
        factory.polygon(
          factory.linear_ring([
            factory.point(-139.474624, 27.919623),
            factory.point(-140.474624, 27.919623),
            factory.point(-140.474624, 28.919623),
            factory.point(-139.974624, 28.419623)]))])
    end
  end
end
