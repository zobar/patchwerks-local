#--
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

module Patchwerks::Local

  #
  # Contains a collection of mock objects.
  #
  class Mock::Collection < Mock::Base
    include Enumerable

    #
    # +geoid+ of these objects' parent.
    #
    attr_reader :parent_geoid

    #
    # Implements the Enumerable interface. Returns one mock object with a
    # +geoid+ of '00'.
    #
    def each
      @item ||= mock_class.new mock_behavior, '00'
      yield @item
    end

    #
    # Creates a new mock collection.
    #
    # [mock_behavior] Determines the behavior of the mock object. One of
    #                 +:pending+ or +:tiger+.
    # [parent_geoid]  +geoid+ of the items' parent.
    #
    def initialize(mock_behavior, parent_geoid)
      super mock_behavior
      @parent_geoid = parent_geoid
    end

    #
    # Orders the objects in the collection by an attribute.
    #
    # [args] Attributes to order by.
    #
    def order(*args)
      self
    end

    #
    # Provides a hint that the items' geometries will be used.
    #
    def with_geometry
      self
    end

    protected

      #
      # Returns the class of the items in this collection.
      #
      def mock_class
        return @mock_class if instance_variable_defined? :@mock_class
        @mock_class = Mock.const_get self.class.name.demodulize.
            sub(/Collection\Z/, '')
      end
  end
end
