#--
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

#
# The preferred way of adding a geometry relation to a model is by including the
# Geometric module like so:
#
#   class City < ActiveRecord::Base
#     include Patchwerks::Local::Geometric
#   end
#
# Geometric does not require any particular columns to be present in the table.
# It makes the following declarations:
#
#   has_one   :shape  # ...
#   validates :shape, presence: true
#   scope     :with_geometry, includes(:shape)
#   delegate  :geometry, :intpt, to: :shape
#
module Patchwerks::Local::Geometric
  extend ActiveSupport::Concern

  included do
    has_one :shape, as:         :owner,
                    class_name: 'Patchwerks::Local::Shape',
                    dependent:  :destroy,
                    inverse_of: :owner

    validates :shape, presence: true

    scope :with_geometry, includes(:shape)

    delegate :geometry, :intpt, to: :shape
  end
end
