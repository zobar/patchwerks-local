#--
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

#
# Include this module to get a two-level cache for dealing with remote files. If
# a remote file has not already been fetched to the local filesystem, it is
# retrieved from either Fog storage or the origin server. The Fog storage is
# configured in +config/patchwerks.yml+. The following configuration stores
# files in an S3 bucket named +devbucket+ in a folder named +cache+:
#
#   development:
#     cache:
#       directory: devbucket
#       prefix:    cache/
#       provider:  aws
#
# Note that you may also need to provide service credentials in
# +config/fog.yml+.
#
module Patchwerks::Local::Cache
  extend ActiveSupport::Concern

  #
  # Operate on a cached file. If the file does not exist on the local
  # filesystem, it is fetched from either the intermediate cache or the origin
  # server. If the file wasn't already in the intermediate cache, it is saved
  # there. This yields the full local path to the requested file.
  #
  # [url]        Origin URL.
  # [local_name] File name in the cache. This may contain directory separators.
  #
  def cached(url, local_name)
    cache_path = "#{cache_prefix}#{local_name}"
    local_path = File.join local_cache_directory, cache_path

    is_intermediate = intermediate_cache.head cache_path
    is_local = File.exists? local_path

    unless is_local
      begin
        FileUtils.mkdir_p File.dirname(local_path)
        File.open local_path, 'wb' do |local|
          if is_intermediate
            local.write intermediate_cache.get(cache_path).body
          else
            Excon.get url, response_block: ->(chunk, remaining, total) do
              local.write chunk
            end
          end
        end
      rescue Exception => error
        File.delete local_path if File.exists? local_path
        raise
      end
    end

    unless is_intermediate
      thread = Thread.new do
        intermediate_cache.create body: File.open(local_path), key: cache_path
      end
    end

    yield local_path

    thread.join unless is_intermediate
  end

  #
  # Returns the cache prefix from the configuration.
  #
  def cache_prefix
    return @cache_prefix if instance_variable_defined? :@cache_prefix
    @cache_prefix = Patchwerks.config[:cache][:prefix]
  end

  #
  # Returns a Fog Files instance representing the intermediate cache.
  #
  def intermediate_cache
    if instance_variable_defined? :@intermediate_cache
      return @intermediate_cache
    end

    @intermediate_cache = if Patchwerks.config.has_key? :cache
      config = Patchwerks.config[:cache]
      directory_name = config[:directory]
      storage = Fog::Storage.new config.except(:directory, :prefix)
      directory = storage.directories.get directory_name
      if directory.nil?
        directory = storage.directories.create key: directory_name
      end
      directory.files
    end
  end

  #
  # Returns the full path to the local cache. This is
  # <tt>{{Rails root}}/tmp/cache/patchwerks</tt>.
  #
  def local_cache_directory
    if instance_variable_defined? :@local_cache_directory
      return @local_cache_directory
    end
    @local_cache_directory = File.join Rails.root, 'tmp', 'cache',
                                       'patchwerks'
  end

end
