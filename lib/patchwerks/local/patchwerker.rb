#--
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

require 'eventmachine'
require 'thor'

#
# The +patchwerker+ processes background jobs related to Patchwerks Local. At
# least one instance must be running for each installation of Patchwerks Local.
# The +patchwerker+ command runs within the context of your Rails application,
# and so it must either be run from inside the Rails root directory, or the
# +--rails-root+ or +-r+ option provided to the command. Available subcommands
# are:
#
# [help]   Provide usage information.
# [queues] Run background queues.
#
class Patchwerks::Local::Patchwerker < Thor

  class_option 'rails-root',
               aliases: ['-r'],
               desc:    'Path to the Rails application directory.',
               default: Dir.getwd

  desc 'queues', 'Run Patchwerks background queues.'
  #
  # Run Patchwerks background queues. This connects to the AMQP message broker
  # specified in your application's +config/patchwerks.yml+ file and processes
  # jobs from all available queues.
  #
  def queues
    EventMachine.run do
      load_rails
      queue = amqp_queue('Patchwerks::Local::TigerFile#dequeue')
      logger.info "PATCHWERKER: Listening for messages"

      queue.subscribe(ack: true) do |message, payload|
        begin
          start = Time.now
          tiger_file = Patchwerks::Local::TigerFile.find(payload)
          logger.info "PATCHWERKER: Loading #{tiger_file.upstream_file}"
          tiger_file.dequeue
        rescue ActiveRecord::RecordNotFound => exception
          log_exception exception
          message.ack
        rescue => exception
          log_exception exception
        else
          elapsed = (Time.now - start).round
          logger.info(
              "PATCHWERKER: Loaded #{tiger_file.upstream_file} in #{elapsed} seconds")
          message.ack
        end
      end
    end
  end

  protected

    #
    # Loads the Rails environment. The Rails app is expected to be rooted at the
    # directory specified by +--rails-root=+ or +-r+ option, if provided, or the
    # current working directory.
    #
    def load_rails
      require File.join(File.absolute_path(options['rails-root']),
                        'config', 'environment')
      self.class.send :include, Patchwerks::Local::Queue
    end

    #
    # Logs a Patchwerker exception in a standard format.
    #
    # [exception] The exception to log
    #
    def log_exception(exception)
      logger.error((["PATCHWERKER: #{exception.class}: #{exception.message}"] +
                    exception.backtrace).join("\n\t"))
    end

    #
    # Retrieves the logger object.
    #
    def logger
      return @logger if instance_variable_defined? :@logger
      @logger = Rails.logger
    end
end
