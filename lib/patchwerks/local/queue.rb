#--
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

require 'amqp'

#
# Include this module in classes that deal with queues to get helpers for queue
# management. Ideally, the only method you would need to use in your class is
# enqueue_direct, which takes care of all of the work of setting up channels,
# queues and exchanges for you.
#
# Although the terminology in this module is AMQP-centric, this module is
# intended to be at least nominally protocol-agnostic. Methods that only make
# sense in the context of AMQP are prefixed with +amqp_+; other methods should
# theoretically be reusable for other protocols.
#
module Patchwerks::Local::Queue
  extend ActiveSupport::Concern

  #
  # RabbitMQ recommends pooling AMQP channels by thread. This returns the
  # current thread's AMQP channel or creates one if it doesn't exist.
  #
  def amqp_channel
    Thread.current[:amqp_channel] ||= AMQP::Channel.new amqp_connection
  end

  #
  # Returns the app's shared AMQP connection, creating one if it does not yet
  # exist. AMQP is added to EventMachine if it is already running; otherwise, it
  # is run inside a new thread. AMQP's configuration is taken from the +amqp+
  # hash of +config/patchwerks.yml+.
  #
  def amqp_connection
    return AMQP.connection unless AMQP.connection.nil?
    config = Patchwerks.config[:amqp].symbolize_keys
    if config.has_key? :ssl
      ssl = config[:ssl] = config[:ssl].symbolize_keys
      if ssl.has_key? :cert_chain_file
        ssl[:cert_chain_file] = Patchwerks.config_file ssl[:cert_chain_file]
      end
      if ssl.has_key? :private_key_file
        ssl[:private_key_file] = Patchwerks.config_file ssl[:private_key_file]
      end
    end

    if EventMachine.reactor_running?
      AMQP.start config
    else
      parent_thread = Thread.current
      Thread.new do
        begin
          AMQP.start config do
            parent_thread.run
          end
        rescue Exception => error
          puts (["#{error.class}: #{error.message}"] + error.backtrace).join(
              "\n\t")
          parent_thread.run
        end
      end
      Thread.stop  # Restarted by AMQP thread when ready
      AMQP.connection
    end
  end

  #
  # Creates an AMQP direct exchange to a particular queue, ensuring that the
  # queue exists as well. If a direct exchange to that queue already exists, the
  # existing exchange is returned.
  #
  # [name] Name of the queue.
  #
  def amqp_direct_exchange(name)
    return amqp_exchanges[name] if amqp_exchanges.has_key? name
    exchange_options = amqp_exchange_options name
    amqp_queue name
    amqp_exchanges[name] = amqp_channel.direct '',
                                               exchange_options.merge(key: name)
  end

  #
  # Return a hash of known AMQP exchanges.
  #
  def amqp_exchanges
    @amqp_exchanges ||= {}.with_indifferent_access
  end

  #
  # Override in your class to provide default options for creating exchanges. If
  # not overridden, no additional options are provided.
  #
  # [name] The name of the exchange that's being created.
  #
  def amqp_exchange_options(name)
    {}
  end

  #
  # Override in your class to provide default options for creating messages. If
  # not overridden, no additional options are provided.
  #
  # [name] The name of the queue the message is going to.
  # [data] The payload of the message.
  #
  def amqp_message_options(name, data)
    {}
  end

  #
  # Creates an AMQP queue.
  #
  # [name] Name of the queue.
  #
  def amqp_queue(name)
    return amqp_queues[name] if amqp_queues.has_key? name
    queue_options = amqp_queue_options(name)
    amqp_queues[name] = amqp_channel.queue name, queue_options
  end

  #
  # Return a hash of known AMQP queues.
  #
  def amqp_queues
    @amqp_queues ||= {}.with_indifferent_access
  end

  #
  # Override in your class to provide default options for creating queues. If
  # not overridden, no additional options are provided.
  #
  # [name] Name of the queue being created.
  #
  def amqp_queue_options(name)
    {}
  end

  #
  # Enqueue a direct message (one-to-one or work queue).
  #
  # [name] Queue name.
  # [data] Message payload.
  #
  def enqueue_direct(name, data)
    message_options = amqp_message_options(name, data)
    amqp_direct_exchange(name).publish(data, message_options)
  end

end
