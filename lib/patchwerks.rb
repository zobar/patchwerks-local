#
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

require 'patchwerks/local'

module Patchwerks

  #
  # Provides unified access to the master configuration file at
  # <Tt>{{Rails root}}/config/patchwerks.yml</tt>. Like +database.yml+,
  # top-level keys are environments.
  #
  def self.config
    return @config if instance_variable_defined? :@config
    configs = YAML.load_file(config_file('patchwerks.yml')).
        with_indifferent_access
    @config = configs.fetch Rails.env, {}
  end

  #
  # Provides unified access to the application configuration directory at
  # <tt>{{Rails root}}/config</tt>.
  #
  def self.config_dir
    return @config_dir if instance_variable_defined? :@config_dir
    @config_dir = File.join Rails.root, 'config'
  end

  #
  # Provides a common method of getting the full path to a configuration file.
  #
  # [path] Path components to the requested file.
  #
  def self.config_file(*path)
    File.join(config_dir, *path)
  end

end
