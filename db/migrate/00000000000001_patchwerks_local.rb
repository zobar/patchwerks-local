#--
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

class PatchwerksLocal < ActiveRecord::Migration
  def up
    #
    # shapes
    #
    create_table :patchwerks_local_shapes do |t|
      t.with_options null: false do |t|
        t.multi_polygon :geometry, srid: 4326
        t.point         :intpt,    srid: 4326
        t.references    :owner,    polymorphic: true
        t.timestamps
      end
    end
    add_index :patchwerks_local_shapes, [:owner_type, :owner_id],
              unique: true
    add_index :patchwerks_local_shapes, :geometry, spatial: true

    #
    # tiger::census_tracts
    #
    create_table :patchwerks_local_tiger_census_tracts do |t|
      t.with_options null: false do |t|
        t.column     :statefp,  'char(2)'
        t.column     :countyfp, 'char(3)'
        t.column     :tractce,  'char(6)'
        t.column     :geoid,    'char(11)'
        t.string     :name,                 limit:  7
        t.string     :namelsad,             limit: 20
        t.column     :aland,    'bigint'
        t.column     :awater,   'bigint'
        t.timestamps
      end
    end
    add_index :patchwerks_local_tiger_census_tracts, :geoid, unique: true
    add_index :patchwerks_local_tiger_census_tracts, [:statefp, :countyfp],
        name: 'index_patchwerks_local_tiger_census_tracts_on_statefp_and_count'

    #
    # tiger::counties
    #
    create_table :patchwerks_local_tiger_counties do |t|
      t.with_options null: false do |t|
        t.column     :statefp,  'char(2)'
        t.column     :countyfp, 'char(3)'
        t.column     :countyns, 'char(8)'
        t.column     :geoid,    'char(5)'
        t.string     :name,                limit: 100
        t.string     :namelsad,            limit: 100
        t.column     :lsad,     'char(2)'
        t.column     :classfp,  'char(2)'
        t.column     :csafp,    'char(3)', null: true
        t.column     :cbsafp,   'char(5)', null: true
        t.column     :metdivfp, 'char(5)', null: true
        t.column     :funcstat, 'char(1)'
        t.column     :aland,    'bigint'
        t.column     :awater,   'bigint'
        t.timestamps
      end
    end
    add_index :patchwerks_local_tiger_counties, :geoid, unique: true
    add_index :patchwerks_local_tiger_counties, :statefp

    #
    # tiger_files
    #
    create_table :patchwerks_local_tiger_files do |t|
      t.with_options null: false do |t|
        t.boolean    :loaded
        t.string     :class_name
        t.string     :upstream_file
        t.timestamps
      end
    end
    add_index :patchwerks_local_tiger_files, :class_name
    add_index :patchwerks_local_tiger_files, :upstream_file, unique: true

    #
    # tiger_states
    #
    create_table :patchwerks_local_tiger_states do |t|
      t.with_options null: false do |t|
        t.string     :region,              limit:   2
        t.string     :division,            limit:   2
        t.column     :statens,  'char(8)'
        t.column     :geoid,    'char(2)'
        t.column     :stusps,   'char(2)'
        t.string     :name,                limit: 100
        t.column     :aland,    'bigint'
        t.column     :awater,   'bigint'
        t.timestamps
      end
    end
    add_index :patchwerks_local_tiger_states, :geoid, unique: true
  end

  def down
    drop_table :patchwerks_local_shapes
    drop_table :patchwerks_local_tiger_census_tracts
    drop_table :patchwerks_local_tiger_counties
    drop_table :patchwerks_local_tiger_files
    drop_table :patchwerks_local_tiger_states
  end
end
