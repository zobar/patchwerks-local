$:.push File.expand_path('../lib', __FILE__)

# Maintain your gem's version:
require 'patchwerks/local/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.authors     = ['David P. Kleinschmidt']
  s.bindir      = 'bin'
  s.description = 'Integrated access to TIGER/Line geospatial data through PostgreSQL/PostGIS from within a Rails app.'
  s.email       = ['david@kleinschmidt.name']
  s.executables = 'patchwerker'
  s.homepage    = 'http://patchwerks.us'
  s.name        = 'patchwerks-local'
  s.summary     = 'Rails engine for accessing TIGER/Line shapefiles through PostgreSQL/PostGIS.'
  s.version     = Patchwerks::Local::VERSION

  s.files = Dir['app/**/*',
                'bin/**/*',
                'config/initializers/**/*',
                'db/**/*',
                'lib/**/*',
                'MIT-LICENSE',
                'Rakefile',
                'README.rdoc']
  s.test_files = Dir['test/**/*']

  s.add_dependency 'activerecord-postgis-adapter'
  s.add_dependency 'amqp'
  s.add_dependency 'fog'
  s.add_dependency 'pg'
  s.add_dependency 'rgeo-shapefile'
  s.add_dependency 'rubyzip'

  s.add_development_dependency 'minitest-rails'
  s.add_development_dependency 'mocha'
  s.add_development_dependency 'simplecov'
end
